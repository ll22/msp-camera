%use HSI to adjust the Hue
%{
This file loads 4 sets of image data (red, green, blue, and wall), and then
apply the same process to each of them. The process is that it add a
constant value to the blue channel of each image, and then it converts each
image from RGB to HSI, and adds a value to the Hue component of each image.
Lastly it converts back the image from HSI to RGB

Dependent modules:
    enhance_Blue();
    RGB2HSI();
    HSI2RGB();
%}
clear;
close all;
clc;
struct_red = load('./data/2015_12_30/RGB_mat_red.mat');
struct_green = load('./data/2015_12_30/RGB_mat_green.mat');
struct_blue = load('./data/2015_12_16/blue1.mat');
struct_wall = load('./data/2015_12_30/original_wall.mat');
RGB_wall = struct_wall.RGB1;
figure;
imshow(RGB_wall);
RGB_wall_en = enhance_Blue(RGB_wall, 85);
HSI_wall = RGB2HSI(RGB_wall_en, 'wall', 0);
HSI2RGB(0.02, HSI_wall, 'wall', 1);

RGB_red = struct_red.struct_red.RGB_mat_red;
RGB_red_en = enhance_Blue(RGB_red, 85);
HSI_red = RGB2HSI(RGB_red_en, 'red', 0);
HSI2RGB(0.02, HSI_red, 'red', 1);

RGB_green = struct_green.struct_green.RGB_mat_green;
RGB_green_en = enhance_Blue(RGB_green, 85);
HSI_green = RGB2HSI(RGB_green_en, 'green', 0);
HSI2RGB(0.02, HSI_green, 'green', 1);


RGB_blue = struct_blue.RGB_mat_blue;
RGB_blue_en = enhance_Blue(RGB_blue, 85);

HSI_blue = RGB2HSI(RGB_blue_en, 'blue', 0);
%convert back to RGB
HSI2RGB(0.02, HSI_blue, 'blue', 1);
