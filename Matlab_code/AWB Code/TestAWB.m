% Test AWB
%{
This file test the AWB algorithm. Uncomment one line from line 12 to line
16 to load in different RGB files. The change range of "p" (the variable defined
before the for loop will affect the quality of the output image)
%}
clear; clc;
close all;

%I = imread('Test.jpg');
%I = imread('wall_original3.png');
%J = imread('TestComparison.jpg');

%struct = load('../data/2015_12_30/wall.mat');
struct = load('../data/2015_12_30/original_wall.mat');
%struct = load('../data/2015_12_30/red.mat');
%struct = load('../data/2015_12_30/green.mat');
%struct = load('../data/2015_12_30/blue.mat');

I = struct.RGB1;
% Calculate Illumination estimation 
p = 91:1:95;

for i=1:length(p)
    figure;
    O = PerformAWB(I, p(i));
    
    subplot(1,2,1), imshow(I);
    subplot(1,2,2), imshow(O);
    %imwrite(O, strcat('AWB_', num2str(p(i)), '.png'));
end




