%{
This file read in the original .dat file, which is contained the content of
the memory we saved from MSP. Then it calculate the gray scale and RGB
matrix. It also display the image. Uncomment one line from line 8 to line
10 to load different .dat file.
%}
ROWS = 256;
COLUMNS = 175;
%fid = fopen('data/2015_12_30/green1.dat');
%fid = fopen('data/2015_12_30/blue1.dat');
fid = fopen('data/2016_1_16/light_2.dat');
%fid = fopen('data/2015_12_30/red.dat');
%fid = fopen('data/2015_12_16/mem_blue_good.dat');
raw_image = fscanf(fid , '%d');
mat_image = zeros(ROWS , COLUMNS);
for i = 1:ROWS
    for j = 1:COLUMNS
        mat_image(i , j) = raw_image((i - 1) * COLUMNS + j);
    end
end

fclose(fid);
figure;
gray_image = mat2gray(mat_image);
imshow(gray_image);
title('gray_scale');

%extract Y value
raw_image_Y = zeros(ROWS*COLUMNS/2,1);
count=1;
for i = 2:2:length(raw_image)
    raw_image_Y(count) = raw_image(i);
    count = count+1;
end

%mat(row, column)
mat_image_Y = zeros(floor(ROWS/2) , COLUMNS);
for i = 1:floor(ROWS/2)%floor(COLUMNS/2)
    for j = 1:floor(COLUMNS)
        mat_image_Y(i , j) = raw_image_Y((i - 1) * COLUMNS + j);
    end
end

figure;
gray_image_Y = mat2gray(mat_image_Y);
imshow(gray_image_Y);
title('Gray Scale Only Y');

%get YUV matrix
%YUV_Col = floor((COLUMNS)/2);
yuv_image = zeros(ROWS/2, COLUMNS, 3);
pixelN = 0;
byteN =1;
for i = 1:ROWS/2 %iterate through columns
    for j = 1:2:COLUMNS %fill in a row
        %arr_2_pixel = [raw_image(byteN), raw_image(byteN+1), raw_image(byteN+2), raw_image(byteN+3)]; %store the next 4 bytes
        if j == 175
            arr_2_pixel = [raw_image(byteN), raw_image(byteN+1), 0, 0]; %store the next 4 bytes
            byteN = byteN + 2;
        else
            arr_2_pixel = [raw_image(byteN), raw_image(byteN+1), raw_image(byteN+2), raw_image(byteN+3)]; %store the next 4 bytes
            byteN = byteN + 4; 
        end
        arr_odd_pixel = [arr_2_pixel(2), arr_2_pixel(1), arr_2_pixel(3)];
        arr_even_pixel = [arr_2_pixel(4), arr_2_pixel(1), arr_2_pixel(3)];       
        for z = 1:3 %fill in the 3 channel of each pixel
            yuv_image(i,j,z) = arr_odd_pixel(z);
            %if j+1 <= 175
                yuv_image(i,j+1,z) = arr_even_pixel(z);
            %end
        end
    end
end
yuv_image = uint8(yuv_image);

%figure;
%imshow(yuv_image(:,:,2));

figure;
imshow(yuv_image);
title('YUV');

figure;
RGB_mat_blue = ycbcr2rgb(yuv_image);
imshow(RGB_mat_blue);
title('RGB');
save_Path = strcat('data/2015_12_30/', 'original_wall');
RGB1 = RGB_mat_blue;
%save(save_Path, 'RGB1');
%save('data/2015_12_30/final_wall');