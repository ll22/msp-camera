function plot_Hue(HSI, color)
    %plot hue
    length = 125*176;
    x = 1:length;
    y = zeros(length, 1);
    count = 1;
    for i = 1:176
        for j = 1:125
            y(count) = HSI(j,i);
            count = count+1;
        end
    end

    figure;
    plot(x, y, color);
    Title = strcat('plot of Hue of the ', color);
    title(Title);

end