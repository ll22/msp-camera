/*
 * Mochamad Prananda
 * MPU + Linker Memory allocation for one OV7690 frame
 * One big array dedicated for a 176*128 QCIF picture frame
 */

#include "driverlib.h"
#include <msp430.h>
#include <stdint.h>

// 176*128*2 = 0xB000 = 45056
#define MAX_LENGTH (0xB000)
// one_frame starts at 0x4F80
#pragma DATA_SECTION(one_frame, ".big_array")
uint8_t one_frame[MAX_LENGTH] = {0x00};
uint8_t done = 0;
/***** MAIN FUNCTION *****/
void main(void) {

	WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer
	PM5CTL0 &= ~LOCKLPM5;					// disable lockdown

    uint16_t i = 0;
    // Filling array with 0x21
    for (; i < MAX_LENGTH; i++)
    {
    	one_frame[i] = 0x21;
    }
    done = one_frame[i-1];
	while(1)
	{}

}
