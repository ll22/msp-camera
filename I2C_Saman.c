void i2c_init(){
	UCB0CTLW0 |= UCSWRST;	                    // Software reset enabled
    UCB0CTLW0 |= UCMODE_3 | UCMST | UCSYNC | UCSSEL__SMCLK | UCTR;	  // I2C mode, Master mode, sync, SMCLK.

    UCB0BRW = 0x003C; 		                    // baudrate = SMCLK / 160 for 16MHz SMCLK it will be 100KHz
//    UCB0I2COA0 = 0x21 | UCOAEN;
	//Load the Address
//	UCB0I2CSA = OV7670_SLAVE_ADD;
    BITCLR(PIN_I2C_SDA_SEL0 , PIN_I2C_SDA);
    BITCLR(PIN_I2C_SCL_SEL0 , PIN_I2C_SCL);
    BITSET(PIN_I2C_SDA_SEL1 , PIN_I2C_SDA);
    BITSET(PIN_I2C_SCL_SEL1 , PIN_I2C_SCL);

    UCB0CTL1 &= ~UCSWRST;
//    UCB0IE = UCTXIE0 | UCNACKIE;             // transmit and NACK interrupt enable
}


void i2c_write(uint8_t addr, uint8_t wrData){

	//Load some data, then do start bit
	TxBuff = addr;
	UCB0I2CSA = OV7670_SLAVE_ADD;

//	UCB0CTL1 |= UCTXSTT;
    while (UCB0CTLW0 & UCTXSTP);            // Ensure stop condition got sent
//	UCB0IFG &= ~UCNACKIFG;
    UCB0CTLW0 |= UCTR | UCTXSTT;            // I2C TX, start condition
    while((UCB0CTLW0 & UCTXSTT));
    while( !(UCB0IFG & UCTXIFG0) ) ;

//    while((UCB0CTLW0 & UCTXSTT));

//    while (UCB0CTLW0 & UCTXSTP);            // Ensure stop condition got sent
//    UCB0CTLW0 |= UCTR | UCTXSTT;            // I2C TX, start condition

    UCB0TXBUF = TxBuff;
    while( !(UCB0IFG & UCTXIFG0) );

    //DEBUG
//	BITSET(P4OUT , BIT2);

	//Do the next byte
	TxBuff = wrData;
    UCB0TXBUF = TxBuff;
//	UCB0IFG &= ~UCNACKIFG;
    while( !(UCB0IFG & UCTXIFG0) ) ;
	UCB0CTLW0 |= UCTXSTP;
//	UCB0IFG &= ~UCNACKIFG;
//	UCB0TXBUF = wrData;
//	UCB0CTL1 |= UCTXSTT;
//	while( !(UCB0IFG & UCTXIFG0) ) {
//
//	}

	//Do the stop bit
												/* set the I2C stop condition							*/
//	while (UCB0CTL1 & UCTXSTP) {											/* stop bit is cleared right after stop is generated	*/
//
//	}

	return;
}