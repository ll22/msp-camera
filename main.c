/*#include <stdio.h>
#include <msp430.h>
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

	printf("Hello World!\n");
	while(1){

	}
}
*/

/*
 * Mochamad Prananda
 * MPU + Linker Memory allocation for one OV7690 frame
 * One big array dedicated for a 176*128 QCIF picture frame
 */

#include <msp430.h>
#include "driverlib/MSP430FR5xx_6xx/driverlib.h"
#include <stdint.h>
#include "camera_Small.h"


// 176*128*2 = 0xB000 = 45056
#define MAX_LENGTH (0xB000)
// one_frame starts at 0x4F80
#pragma DATA_SECTION(one_frame, ".big_array")
uint8_t one_frame[MAX_LENGTH] = {0x00};

#define DMA_NUM 44800 //175*128*2
#define DMA_SOURCE_PORT4_ADDR 0x0221 //address of port 4 in
#define DMA_SOURCE_PORT3_ADDR 0x0220 //address of port 3 in
#define DMA_DEST_ADDR 0x4F80

void i2c_write(uint8_t byteAddr, uint8_t data)
{

	UCB0I2CSA = 0x21;//camAddr_W; 								// set slave address
	while(UCB0CTLW0 & UCTXSTP);
	UCB0CTLW0 |= UCTR | UCTXSTT;							// put in transmitter mode and send start bit

	while(UCB0CTLW0 & UCTXSTT);
	while (!(UCB0IFG & UCTXIFG0)); 							// wait for previous tx to complete

	//UCB0TXBUF = TxBuff;	 // setting TXBUF clears the TXIFG flag
	UCB0TXBUF = byteAddr;
	while (!(UCB0IFG & UCTXIFG0));
	//__delay_cycles(200); //try

	//TxBuff = data;
	//UCB0TXBUF = TxBuff; 										// setting TXBUF clears the TXIFG flag
	UCB0TXBUF = data;
	while (!(UCB0IFG & UCTXIFG0)); 							// wait for previous tx to complete


	UCB0CTLW0 |= UCTXSTP; 									// I2C stop condition
	while (UCB0CTLW0 & UCTXSTP);
}

//send SA, send byteAddr, recieve data,
uint8_t i2cReadFromReg(uint8_t byteAddr)
{
	uint8_t data = 0;

	/* **** WRITING TO CAMERA WRITE ADDRESS TO LOOK FOR REG **** */
	UCB0I2CSA = 0x21;//camAddr_W; 									// set slave address

	UCB0CTLW0 |= UCTR | UCTXSTT;							// put in transmitter mode and send start bit
	while (!(UCB0IFG & UCTXIFG0) | (UCB0CTLW0 & UCTXSTT)); 	// wait for previous tx to complete

	UCB0TXBUF = byteAddr; 									// setting TXBUF clears the TXIFG flag
	while (!(UCB0IFG & UCTXIFG0) | (UCB0CTLW0 & UCTXSTT)); 	// wait for previous tx to complete

	UCB0CTLW0 |= UCTXSTP;
	while (UCB0CTLW0 & UCTXSTP);


	/* **** WRITING TO CAMERA READ ADDRESS TO RECEIVE FROM REG **** */

	UCB0CTLW0 &= ~UCTR;										//change to receiver mode

	UCB0CTLW0 |= UCTXSTT;									// send start bit

	while (!(UCB0IFG & UCRXIFG0));							// wait for receive flag

	UCB0CTLW0 |= UCTXSTP; 									// I2C stop condition

	data = UCB0RXBUF;										// get the data

	while (UCB0CTLW0 & UCTXSTP);							// wait until stopping

	return data;
}

void i2c_init()
{
	__delay_cycles(200000);
	P1SEL1 |= BIT6 | BIT7;                         			// Assign I2C pins to SCL/SDA
	//P1SEL0 &= ~(BIT6 | BIT7);								// Selection mode P1SEL1=1 and P1SEL0=0, so mode 0b10 => SCL/SDA

	UCB0CTLW0 |= UCSWRST;                  				// Enable SW reset (reset state machine)
	//UCB0CTL1 |= UCSWRST;   //from datasheet
	UCB0CTLW0 |= (UCMST | UCMODE_3 | UCSYNC);     						// I2C Master, synchronous mode
	UCB0CTLW0 |= (UCSSEL__SMCLK | UCTR);            						// Use SMCLK, keep SW reset
	//UCB0BRW = 0x1E;
	// fSCL = SMCLK/120, for example
	UCB0BRW = 120;
	//UCB0BRW = 0x3C; // 60
	//UCB0BRW = 0x1E; //30
	UCB0CTLW0 &= (~UCSWRST);                    				// Clear SW reset, resume operation
	//UCB0CTL1 =  &= ~UCSWRST;
	 __delay_cycles(5000000);
}

void small_Camera_Init(){

	//PCLK settings 6 MHz for 24MHz input clock.
    // It is possible to increase the clock up to 8MHz but needs some post computation to extract the correct image.
    i2c_write(REG12 , 0x80);	// System reset

    volatile int i;
    for(i = 0 ; i < 10 ; i++){
    	__delay_cycles(2400);
    }

    i2c_write(REG12 , 0x00);	// System UN-RESET

    for(i = 0 ; i < 10 ; i++){
    		__delay_cycles(2400);
    }
    i = 0;
	i2c_write(PWC0 , 0x0D);	// DOVDD range selection, in this case it is 2.7V

	i2c_write(REG0C , 0x06);	// Set Data, PCLK, HREF and VSYNC pins to output.
	i2c_write(REG3E , 0x70);	// Suppress PCLK on horiz blank. Set PCLK of YUV output double the PCLK of RAW output.
	//i2c_write(REG3E , 0x60); //cc1

	// Frame rate adjustment: 30fps for 24MHz input clock.
	//i2c_write(CLKRC, 0x01);		// divide by 2
	i2c_write(CLKRC, 0x03);
	//i2c_write(CLKRC, 0x11);		// divide by 4  (24/4 = 6MHz) cc2
	//i2c_write(0x29, 0x50);
	i2c_write(PLL, 0x02); 		// bypass PLL
	//i2c_write(REG12 , 0x00);	// Set the output format to YUV. Subsampling disabled.
	//i2c_write(REG12 , 0x01);  //bayer raw cc3

	//i2c_write(0x82, 0x11);//cc3 output option YUV422
	i2c_write(REG12, 0x06); // [bit 3:2 : 01 => RGB565, bit 1:0 : 10 => RGB]
	i2c_write(REG15 , 0x03);

	i2c_write(REGC8, 0x02);
	i2c_write(REGC9, 0x0D);
	i2c_write(REGCA, 0x01);
	i2c_write(REGCB, 0xB0);

	i2c_write(REGCC, 0x00);
	i2c_write(REGCD, 0xAF);
	i2c_write(REGCE, 0x00);
	i2c_write(REGCF, 0x90);

}
/*freq: 24 ==> 24MHz
  freq: 16 ==> 16MHz
  freq: 8 ==> 8MHz
*/
void DCO_Conf(int freq)
{
    //SET CLOCK SURCE
    //CSCTL0_H = CSKEY;
    CSCTL0_H = 0xA5;       // Unlock CS registers
    switch(freq){
        case 24:
            CSCTL1_L = (DCOFSEL_6 | DCORSEL); //set DCO frequency 24 MHz
            FRCTL0 = 0xA500 | ((1) << 4);  //FRCTLPW | NWAITS_1;
            break;
        case 8:
            CSCTL1_L = DCOFSEL_6;
            CSCTL1_L &= ~(DCORSEL); //set DCO frequency 8 MHz
            break;
        case 16:
            CSCTL1_L = DCOFSEL_4; //DCOFSEL_4 == 0x8
            CSCTL1_L |= DCORSEL; //set DCO frequency 16 MHz
            FRCTL0 = 0xA500 | ((1) << 4);  //FRCTLPW | NWAITS_1;
            break;
    }
    CSCTL2_L = (0b0110011); //set the source of MCLK and SMCLK to be DCO
    CSCTL3_L = DIVS__1;        //SMCLK/1 and MCLK/1 Page:
    //CSCTL3_L = 0x0010; //SMCLK/2 and MCLK/1
    //CSCTL3_L = 0x0020; //SMCLK/4 and MCLK/1

    CSCTL4_L &= 0b01;    //SMCLK ON

    //SET I/O port Pin27 port3.4
    //try 10b ==> secondeary function
    /*P3DIR |= 0x010; // Set P3.4 to output direction
    P3SEL0 |= 0x0010; //P3SEL0.4 = 1
    P3SEL1 |= 0x0010;//P3SEL1.4 = 0*/

    //SMCLK on Pin12 PJ.0
    PJDIR |= 0x01;
    PJSEL0 &= 0x00;
    PJSEL1 &= 0x00;
    PJSEL0 |= 0x01;
    PJSEL1 |= 0x00;

}

//configure interrupt on port1, pin 4, hight-to-low trigger
void P1_4_interrupt_Conf()
{
    //Stop watchdog timer
    //WDT_A_hold(WDT_A_BASE);

    //Set P1.0 to output direction
    GPIO_setAsOutputPin(
        GPIO_PORT_P1,
        GPIO_PIN0
        );

    //Enable P1.4 internal resistance as pull-Up resistance
    GPIO_setAsInputPinWithPullUpResistor(
        GPIO_PORT_P1,
        GPIO_PIN4
        );

    //P1.4 interrupt enabled
    GPIO_enableInterrupt(
        GPIO_PORT_P1,
        GPIO_PIN4
        );

    //P1.4 Hi/Lo edge
    GPIO_selectInterruptEdge(
        GPIO_PORT_P1,
        GPIO_PIN4,
        //GPIO_HIGH_TO_LOW_TRANSITION
		GPIO_LOW_TO_HIGH_TRANSITION
        );

    //P1.4 IFG cleared
    GPIO_clearInterrupt(
        GPIO_PORT_P1,
        GPIO_PIN4
        );

}


//configure interrupt on port1, pin 5, hight-to-low trigger
void P1_5_interrupt_Conf()
{
    //Enable P1.5 internal resistance as pull-Up resistance
    GPIO_setAsInputPinWithPullUpResistor(
        GPIO_PORT_P1,
        GPIO_PIN5
        );

    //P1.5 interrupt enabled
    GPIO_enableInterrupt(
        GPIO_PORT_P1,
        GPIO_PIN5
        );

    //P1.5 Hi/Lo edge
    GPIO_selectInterruptEdge(
        GPIO_PORT_P1,
        GPIO_PIN5,
        //GPIO_HIGH_TO_LOW_TRANSITION
		GPIO_LOW_TO_HIGH_TRANSITION
        );

    //P1.5 IFG cleared
    GPIO_clearInterrupt(
        GPIO_PORT_P1,
        GPIO_PIN5
        );
}

//configure interrupt on port2, pin1, hight-to-low trigger
void P2_7_interrupt_Conf()
{
    //Enable P2.7 internal resistance as pull-Up resistance
    GPIO_setAsInputPinWithPullUpResistor(
        GPIO_PORT_P2,
        GPIO_PIN7
        );

    //P2.1 interrupt enabled
    GPIO_enableInterrupt(
        GPIO_PORT_P2,
        GPIO_PIN7
        );

    //P2.1 Hi/Lo edge
    GPIO_selectInterruptEdge(
        GPIO_PORT_P2,
        GPIO_PIN7,
        //GPIO_HIGH_TO_LOW_TRANSITION
		GPIO_LOW_TO_HIGH_TRANSITION
        );

    //P2.1 IFG cleared
    GPIO_clearInterrupt(
        GPIO_PORT_P2,
        GPIO_PIN7
        );
}

//configure interrupt on port2, pin2, low-to-high trigger
void P2_2_interrupt_Conf()
{
    //Enable P2.2 internal resistance as pull-Up resistance
    GPIO_setAsInputPinWithPullUpResistor(
        GPIO_PORT_P2,
        GPIO_PIN2
        );

    //P2.2 interrupt enabled
    GPIO_enableInterrupt(
        GPIO_PORT_P2,
        GPIO_PIN2
        );

    //P2.2 Hi/Lo edge
    GPIO_selectInterruptEdge(
        GPIO_PORT_P2,
        GPIO_PIN2,
        //GPIO_HIGH_TO_LOW_TRANSITION
		GPIO_LOW_TO_HIGH_TRANSITION
        );

    //P2.2 IFG cleared
    GPIO_clearInterrupt(
        GPIO_PORT_P2,
        GPIO_PIN2
        );
}

//configure interrupt on port2, pin2, low-to-high trigger
void P3_6_interrupt_Conf()
{
    //Enable P2.1 internal resistance as pull-Up resistance
    GPIO_setAsInputPinWithPullUpResistor(
        GPIO_PORT_P3,
        GPIO_PIN6
        );

    //P2.1 interrupt enabled
    GPIO_enableInterrupt(
        GPIO_PORT_P3,
        GPIO_PIN6
        );

    //P2.1 Hi/Lo edge
    GPIO_selectInterruptEdge(
        GPIO_PORT_P3,
        GPIO_PIN6,
        //GPIO_HIGH_TO_LOW_TRANSITION
		GPIO_LOW_TO_HIGH_TRANSITION
        );

    //P2.1 IFG cleared
    GPIO_clearInterrupt(
        GPIO_PORT_P3,
        GPIO_PIN6
        );
}

void enable_All_Interrupts()
{
    P1_5_interrupt_Conf();
}

void disable_All_Interrupts()
{
	GPIO_disableInterrupt(
	        GPIO_PORT_P3,
	        GPIO_PIN6
	);

	GPIO_disableInterrupt(
	        GPIO_PORT_P1,
	        GPIO_PIN4
	);

	GPIO_disableInterrupt(
	        GPIO_PORT_P1,
	        GPIO_PIN5
	);

	GPIO_disableInterrupt(
	        GPIO_PORT_P2,
	        GPIO_PIN7
	);

	GPIO_disableInterrupt(
	        GPIO_PORT_P2,
	        GPIO_PIN2
	);
}


void GPIOP4_conf()
{

	P4DIR &= ~(0xFF); //port 4 all as input
}

void GPIOP3_conf()
{

    P3DIR &= ~(0xFF); //port 3 all as input
}

void general_Init()
{
    WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer
    PM5CTL0 &= ~LOCKLPM5;                   // Disable the GPIO power-on default high-impedance mode
    									  	   // to activate previously configured port setting
}

//useful URL: https://e2e.ti.com/support/microcontrollers/msp430/f/166/t/335859
//source addr: P4IN
//destination addr: FRAM starting at
//Transfer mode: Single Transfer
//Address mode: FA -> BA
void DMA_Conf()
{

	  //P1SEL1 |= BIT0;
	  //P1SEL0 &= ~(BIT0); //pinmux p1.0 to be DMA trigger pin

	 // P1SEL1 |= BIT0;
	 // P1SEL0 &= ~(BIT0); //pinmux p1.0 to be DMA trigger pin
	  // Configure DMA channel 0
	  //__data16_write_addr((unsigned short) &DMA0SA,(unsigned long) 0x1C20);
	  //__data16_write_addr((unsigned short) &DMA0SA, DMA_SOURCE_PORT4_ADDR);
      __data16_write_addr((unsigned short) &DMA0SA, DMA_SOURCE_PORT3_ADDR);

	                                            // Source block address
	  __data16_write_addr((unsigned short) &DMA0DA, DMA_DEST_ADDR);
	                                            // Destination single address
	  DMA0SZ = DMA_NUM;                              // Block size
	  //DMA0CTL
	  DMA0CTL = DMADT_0 | DMASRCINCR_0 | DMADSTINCR_3 | DMASRCBYTE | DMADSTBYTE; // Rpt, inc

	  //DMA_TRIGGERSOURCE_26
	  //DMAIV |= 0x02;

	  DMACTL0 |= DMA0TSEL_31;//0x01; //try trigger source 0

	  //set edge-sensitive inerrupt
//	  DMA0CTL &= ~(DMALEVEL); //edge sensitive
	  //DMA0CTL |= DMAIE; //interrupt enable
	  //DMA0CTL |= DMAEN;                         // Enable DMA0
}

void reconf_DMA(int cam_Num){
	if(cam_Num == 1){ //set port3 as source address
		__data16_write_addr((unsigned short) &DMA0SA, DMA_SOURCE_PORT3_ADDR);
	}else{ //set port4 as source address
		__data16_write_addr((unsigned short) &DMA0SA, DMA_SOURCE_PORT4_ADDR);
	}
    	  //__data16_write_addr((unsigned short) &DMA0SA, DMA_SOURCE_PORT3_ADDR);

	                                            // Source block address
	  __data16_write_addr((unsigned short) &DMA0DA, DMA_DEST_ADDR);
	                                            // Destination single address
	  DMA0SZ = DMA_NUM;                              // Block size
	  //DMA0CTL
	  DMA0CTL = DMADT_0 | DMASRCINCR_0 | DMADSTINCR_3 | DMASRCBYTE | DMADSTBYTE; // Rpt, inc

	  //DMA_TRIGGERSOURCE_26
	  //DMAIV |= 0x02;

	  DMACTL0 |= DMA0TSEL_31;//0x01; //try trigger source 0
}


void UART_Conf(){
	  // Configure GPIO
	  P2SEL1 |= BIT0 | BIT1;                    // USCI_A0 UART operation
	  P2SEL0 &= ~(BIT0 | BIT1);

	  // Configure USCI_A0 for UART mode
	  UCA0CTLW0 = UCSWRST;                      // Put eUSCI in reset
	  UCA0CTLW0 |= UCSSEL__SMCLK;               // CLK = SMCLK

	  //UCA0BR0 = 52;                             // 8000000/16/9600
	  UCA0BR0 = 78;//156;                             // 12000000/16/9600
	  UCA0BR1 = 0x00;
	  UCA0MCTLW |= UCOS16 | UCBRF_1;
	  UCA0CTLW0 &= ~UCSWRST;                    // Initialize eUSCI
}


void UART_send_Byte(uint8_t byte)//unsigned char byte)
{
	//UCA0TXBUF = byte;
	//__delay_cycles(4000);
	while( !(UCTXIFG & UCA0IFG) );
	UCA0TXBUF = byte;
}

void UART_self_Check()
{
	UART_send_Byte('R');
	UART_send_Byte('e');
	UART_send_Byte('a');
	UART_send_Byte('d');
	UART_send_Byte('y');
	UART_send_Byte('\n');
}

void send_Frame_To_PC( uint8_t* start_Addr, int byte_Num)
{
	int i;
	//byte_Num = byte_Num*2;
	for(i=0; i<byte_Num; i++){
		UART_send_Byte( *(start_Addr+i) );
	}
}

void camera_Toggle_Pins_Init(){

	P1DIR |= 0x02; //Set P1.1 to output direction
	P1DIR |= 0x04; //Set P1.2 to output direction
    P1OUT |= 0x02; //P1.1 high
    P1OUT |= 0x04; //P1.2 high
}


/*
Initially
    //Upper camera on
    P1OUT |= (0x02); //P1.1 low
    P1OUT &= ~(0x04); //P1.2 low
*/
void toggle_Cam(){
	static int cam_Num = 0;
	if(cam_Num == 0){
		//upper camera ON
		P1OUT |= (0x02); //P1.1 high
		__delay_cycles(200);
		P1OUT &= ~(0x04); //P1.2 low
		cam_Num = 1;
		reconf_DMA(1); //set port 3 as the source address
		//__data16_write_addr((unsigned short) &DMA0SA, DMA_SOURCE_PORT3_ADDR);

	}else{ //cam_Num == 1
		//lower camera ON
		P1OUT |= (0x04); //P1.2 high
		__delay_cycles(200);
		P1OUT &= ~(0x02); //P1.1 low
		cam_Num = 0;
		reconf_DMA(0); //set port 4 as the source address
		//__data16_write_addr((unsigned short) &DMA0SA, DMA_SOURCE_PORT4_ADDR);

	}
}

void test_LED_Init(){
	P2DIR |= 0x80; //P2.7 direction out
	P2OUT |= 0x80;
}

void test_LED_Flash(){
	P2OUT ^= 0x80;
	__delay_cycles(10000);
}


//global variables
int data = 0;
int frame_Start = 0;
int frame_Finished = 0;
int frame_Count=0;
//main--------------------------------------------------------------
//------------------------------------------------------------------
//------------------------------------------------------------------
int main(void) {


	general_Init(); //disable the LM5 mode and watch dog timer
	GPIOP4_conf();
	GPIOP3_conf();

    DCO_Conf(24); //configure the clock to MCLK to be 24MHz
    i2c_init();
    camera_Toggle_Pins_Init();

    P1OUT &= ~(0x02); //P1.1 low
    P1OUT &= ~(0x04); //P1.2 low

    __delay_cycles(100);
    small_Camera_Init(); //configure 2 cameras at the same time

    toggle_Cam(); //initially will turn the upper one ON

	P1SEL1 |= BIT0;
	P1SEL0 &= ~(BIT0); //pinmux p1.0 to be DMA trigger pin
	DMA_Conf(); //reinitialize DMA channel 0 and reload the DMA0SZ register
    enable_All_Interrupts(); //enable all 5 interrupts
    __bis_SR_register(GIE);

    test_LED_Init(); //initi GPIO P2.7
    while(1){


    	if(frame_Finished == 5){
    			test_LED_Flash();
    			frame_Finished = 0;
    			toggle_Cam();
    			P1_5_interrupt_Conf();
    		}
    }
    return 0;
}


//******************************************************************************
//
//This is the PORT1_VECTOR interrupt vector service routine
//
//******************************************************************************
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT1_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(PORT1_VECTOR)))
#endif
void Port_1(void)
{
	//port 2.7 H to L
	//if(frame_Finished == 0){
		//if( ((P2IN >> 7) & 1) == 0){ //currently Low (High to Low) falling edge of VSYNC
	volatile int i;
	if(frame_Finished >= 4){
		frame_Finished = 4;
		//disable interrupt
		//for(i=0; i<=20000; i++); //some delay

		DMA0CTL |= DMAIE; //interrupt enable
		DMA0CTL |= DMAEN; // Enable DMA0
		GPIO_disableInterrupt(
				GPIO_PORT_P1,
				GPIO_PIN5
		);
	}else{
		frame_Finished++;
	}
    //P1.5 IFG cleared
    GPIO_clearInterrupt(
    		 GPIO_PORT_P1,
         GPIO_PIN5
         );
}

//******************************************************************************
//
//This is the PORT1_VECTOR interrupt vector service routine
//
//******************************************************************************
//DMA ISR
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=DMA_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(DMA_VECTOR)))
#endif
void DMA_ISR(void) //after the DMA0SZ counter counts to 0, this interrupt happens
{
	P3OUT &= ~(0x04); //turn it off at the end of the frame we are taking data of
	//DMAIV &= ~(0x02); //clear the flag
	DMA0CTL &= ~(DMAIE); //interrupt disable
	//DMA0CTL &= ~(DMAEN); //disable DMA0
	//enable interrupt
	//frame_Start=0;
	//frame_Finished=0;
	frame_Finished++;
	//P2_2_interrupt_Conf();
	//P1_5_interrupt_Conf();
}
