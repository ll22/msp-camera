/*
 * =====================================================================================
 *
 *       Filename:  display_some_image.cpp
 *
 *    Description:  Converts RGB to Gray, saves RGB vectors to a file
 					Things to note, it's BGR order in OpenCV
 *
 *        Version:  1.0
 *        Created:  01/11/2016 09:16:39 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Mochamad Prananda (), 
 *   Organization:  Sensor Systems Lab
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>


// Function prototypes
void saveRGBToFile (cv::Mat* image);

int main(int argc, char** argv)
{
	cv::Mat image;
	image = cv::imread( argv[1], 1);
	
	
	if (!image.data)
	{
		std::cout << "No image data" << std::endl;
	}
	cv::Mat gray;

	saveRGBToFile(&image);
	cv::cvtColor(image, gray, CV_BGR2GRAY);

	
	// Showing RGB image
	cv::imshow("Display Image", image);

	// Showing gray
	cv::imshow("Display Gray Image", gray);

	cv::waitKey(0);

	return 0;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  saveRGBToFile
 *  Description:  save RGB to File
 * =====================================================================================
 */
void saveRGBToFile (cv::Mat* image)
{
	cv::Mat bgr[3];
	cv::split(*image,bgr);

	/* 3-Channel Splits Destinations */
	cv::Mat zeroes(image->rows, image->cols, CV_8UC1); // zero padding matrix
	cv::Mat green(image->rows, image->cols, CV_8UC3);	// 3-channel matrix, unsigned 8-bit
	cv::Mat blue(image->rows, image->cols, CV_8UC3);	// 3-channel matrix, unsigned 8-bit
	cv::Mat red(image->rows, image->cols, CV_8UC3);		// 3-channel matrix, unsigned 8-bit

	// Forming 3-Channel Sources
	cv::Mat fromBlue[] = {bgr[0], zeroes, zeroes};
	cv::Mat fromGreen[] = {zeroes, bgr[1], zeroes};
	cv::Mat fromRed[] = {zeroes, zeroes, bgr[2]};

	// Channels: source[0] -> destination[0], source[1] -> destination[1], source[2] -> destinationp[2]
	int fromTo[] = {0,0,1,1,2,2};

	cv::mixChannels(fromBlue, 3, &blue, 1, fromTo, 3);
	cv::mixChannels(fromGreen, 3, &green, 1, fromTo, 3);
	cv::mixChannels(fromRed, 3, &red, 1, fromTo, 3);

	// Display 3-channeled matrices
	cv::imwrite("blue.png", blue);
	cv::imwrite("green.png", green);
	cv::imwrite("red.png", red);

	std::string str[3];
	str[0] = "blue.txt";
	str[1] = "green.txt";
	str[2] = "red.txt";

	std::string names[3];
	names[0] = "blueImage";
	names[1] = "greenImage";
	names[2] = "redImage";


	for (uint8_t i = 0; i < 3; i++)
	{
		cv::FileStorage file(str[i], cv::FileStorage::WRITE);
		file << names[i] << bgr[i];
	}	
}		
/* -----  end of function saveMatToFile  ----- */

