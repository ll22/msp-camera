temp1 = 126
temp2 = 209
print('temp1: ' + str(temp1) + ' ' + bin(temp1))
print('temp2: ' + str(temp2) + ' ' + bin(temp2))

red = (temp1 & 0xF8) >> 3
green = (temp1 & 0x7) << 3 | (temp2 & 0xE0) >> 5
blue = (temp2 & 0x1F)

print('red: ' + str(red) + ' ' + bin(red))
print('green: ' + str(green) + ' ' + bin(green))
print('blue: ' + str(blue) + ' ' + bin(blue))