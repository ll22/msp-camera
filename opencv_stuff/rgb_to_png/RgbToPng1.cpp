/*
 * =====================================================================================
 *
 *       Filename:  RgbToPng.cpp
 *
 *    Description:  Saves RGB565 color space formatto PNG from OV7690 camera.
 					Image dimension 175*128 pixels.
 					Sequence of the data from OV7590:

 					D[byte_order][bit_order]
 *					D17 D16 D15 D14 D13 | D12 D11 D10 D27 D26 D25 | D24 D23 D22 D21 D20
 *							R                        G                       B
 *
 *        Version:  1.0
 *        Created:  01/11/2016 09:16:39 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Mochamad Prananda (), 
 *   Organization:  Sensor Systems Lab
 *
 * =====================================================================================
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>


// Function prototypes
void saveRGBToFile (cv::Mat* image);
static uint8_t ROW = 128;
static uint8_t COL = 175;

int main(int argc, char** argv)
{
	// Checks arguments
    if (argc < 2) 
    {
        std::cout << "Bruh!!" << std::endl;
        exit(-1);
    }
    std::ifstream inFile(argv[1]);
    
    // Exit if file not found
    if (!inFile)
    {
        std::cerr << "File not found" << std::endl;
        exit(-1);
    }
    

    // Matrices to save RGB
    cv::Mat zeroes(ROW, COL, CV_8UC1);
    cv::Mat blue(ROW, COL, CV_8UC1);
    cv::Mat green(ROW, COL, CV_8UC1); 
    cv::Mat red(ROW, COL, CV_8UC1); 
    cv::Mat final_image(ROW, COL, CV_8UC3);		// To save the final image
    std::string line;
    
    
    int temp1 = 0;		// byte1
    int temp2 = 0;		// byte2
    uint8_t blue_val = 0;	
    uint8_t red_val = 0;
    uint8_t green_val = 0;

    uint8_t b_8 = 0;
    uint8_t r_8 = 0;
    uint8_t g_8 = 0;

    uint8_t byteCounter = 0;	// to get every other byte
    int rowCounter = 0;			// to keep track of row pixels
    int colCounter = 0;			// to keep track of column pixels

    // get each line on file
    while(getline(inFile, line))
    {
    	// skips newlines
        if (line.empty())
            continue;
        
        // converts from line to integer
        std::istringstream iss(line);
        
        // handles first byte
        if (byteCounter == 0)  
        {
        	iss >> temp1;
        	byteCounter++;
        }
        else
        {	
        	// handles the first and second byte
        	byteCounter = 0;
        	iss >> temp2;

        	//std::cout << "Col: " << colCounter << std::endl;
        	//std::cout << "Row: " << rowCounter << std::endl;
        	uint8_t red_val = (temp1 & 0xF8) >> 3;
        	uint8_t green_val = (temp1 & 0x7) << 3 | (temp2 & 0xE0) >> 5;
        	uint8_t blue_val = (temp2 & 0x1F);

        	// Translates into individual R G and B values
        	/*uint8_t red_val = (temp1 & 0xF8) >> 3;
        	uint8_t green_val = (temp1 & 0x7) << 3 | (temp2 & 0xE0) >> 5;
        	uint8_t blue_val = (temp2 & 0x1F);*/

        	/*b_8 = (uint8_t)((blue_val << 3) | ((blue_val & 0x1C)>>2));
        	g_8 = (uint8_t)((green_val << 2) | ((green_val & 0x30) >> 4));
        	r_8 = (uint8_t)((red_val << 3) | ((red_val & 0x1C)>>2));*/
        	
        	b_8 = (uint8_t)((255/31)*blue_val);
        	g_8 = (uint8_t)((255/63)*green_val);
        	r_8 = (uint8_t)((255/31)*red_val);
        	/*b_8 = blue_val;
        	g_8 = green_val;
        	r_8 = red_val;*/

        	/*std::cout << "---------------" << rowCounter << std::endl;
        	std::cout << "Val1: " << temp1 << std::endl;
        	std::cout << "Val2: " << temp2 << std::endl;
        	std::cout << "Red: " << (int)r_8 << std::endl;
        	std::cout << "Green: " << (int)g_8 << std::endl;
        	std::cout << "Blue: " << (int)b_8 << std::endl;
        	std::cout << std::endl;*/

        	// Save individual R, G, and B values into its own matrix
        	red.at<uint8_t>(rowCounter, colCounter) = r_8;
        	blue.at<uint8_t>(rowCounter, colCounter) = b_8;
        	green.at<uint8_t>(rowCounter, colCounter) = g_8;

        	// Handles the positioning of the matrices
        	if (colCounter == COL-1) 
        	{
        		rowCounter++;
        		colCounter = 0;
        	} 
        	else
        	{
        		colCounter++;
        	}
        }
    }
     

        //cv::imshow("Display", final_image);
       // cv::waitKey(0);

    // Combine individual R, G, and B values into one 3-channel matrix
    cv::Mat raw_picture[] = {blue, green, red};
	int fromTo[] = {0,0,1,1,2,2};
	cv::mixChannels(raw_picture, 3, &final_image, 1, fromTo, 3);
	// Save image to PNG
	cv::imwrite("final.png", final_image);

	// Save matrix contents into txt files
	std::string str[3];
	str[0] = "blue.txt";
	str[1] = "green.txt";
	str[2] = "red.txt";

	std::string names[3];
	names[0] = "blueImage";
	names[1] = "greenImage";
	names[2] = "redImage";
	for (uint8_t i = 0; i < 3; i++)
	{
		cv::FileStorage file(str[i], cv::FileStorage::WRITE);
		file << names[i] << raw_picture[i];
	}

	// Display the final image real time
 	cv::imshow("Final Image", final_image);
 	cv::waitKey(0);

	return 0;
}


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  saveRGBToFile
 *  Description:  Separates an RGB color-spaced image and save each color space to a PNG and txt file
 * =====================================================================================
 */
void saveRGBToFile (cv::Mat* image)
{
	cv::Mat bgr[3];
	cv::split(*image,bgr);

	/* 3-Channel Splits Destinations */
	cv::Mat zeroes(image->rows, image->cols, CV_8UC1); // zero padding matrix
	cv::Mat green(image->rows, image->cols, CV_8UC3);	// 3-channel matrix, unsigned 8-bit
	cv::Mat blue(image->rows, image->cols, CV_8UC3);	// 3-channel matrix, unsigned 8-bit
	cv::Mat red(image->rows, image->cols, CV_8UC3);		// 3-channel matrix, unsigned 8-bit

	// Forming 3-Channel Sources
	cv::Mat fromBlue[] = {bgr[0], zeroes, zeroes};
	cv::Mat fromGreen[] = {zeroes, bgr[1], zeroes};
	cv::Mat fromRed[] = {zeroes, zeroes, bgr[2]};

	// Channels: source[0] -> destination[0], source[1] -> destination[1], source[2] -> destinationp[2]
	int fromTo[] = {0,0,1,1,2,2};

	cv::mixChannels(fromBlue, 3, &blue, 1, fromTo, 3);
	cv::mixChannels(fromGreen, 3, &green, 1, fromTo, 3);
	cv::mixChannels(fromRed, 3, &red, 1, fromTo, 3);

	// Display 3-channeled matrices
	cv::imwrite("blue.png", blue);
	cv::imwrite("green.png", green);
	cv::imwrite("red.png", red);

	std::string str[3];
	str[0] = "blue.txt";
	str[1] = "green.txt";
	str[2] = "red.txt";

	std::string names[3];
	names[0] = "blueImage";
	names[1] = "greenImage";
	names[2] = "redImage";


	for (uint8_t i = 0; i < 3; i++)
	{
		cv::FileStorage file(str[i], cv::FileStorage::WRITE);
		file << names[i] << bgr[i];
	}	
}		
/* -----  end of function saveMatToFile  ----- */

