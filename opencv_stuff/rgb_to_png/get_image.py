'''
	get_image.py
	Mochamad Prananda
	Processes the picture raw file into an RGB color space PNG picture.
	
	Data format:
	D[byte_order][bit_order]
	D17 D16 D15 D14 D13 | D12 D11 D10 D27 D26 D25 | D24 D23 D22 D21 D20
 			R                        G                       B
'''


import time
import pygame

'''
Reads a picture file from computer and translates it into an unprocessed matrix
'''
def readPic():
	f = open('result_2.dat')
	# Creating an empty matrix
	imagebuf = [[0 for x in range(image_width*2)] for x in range(image_height)] 
	each_row = []

	# Put each byte inside a matrix
	for i in range(0, image_height):
		for j in range(0, image_width*2):
			line = f.next().strip()
			imagebuf[i][j] = line

	return imagebuf

'''
Translates each two bytes into each R G B value 
'''
def rgbToPic(byte1, byte2):
	byte12 = byte1 << 8 | byte2

	red = byte12 >> 11
	green = (byte12 >> 5) & 0x3f
	blue = byte12 & 0x1f
    
    # Linear mapping by a scale of 255/((2^n)-1))
	# where n is the amount of bits in each R G B value
	red *= 8
	green *= 4
	blue *= 8

	return (red, green, blue)

'''
Saves each pixel to a 175x128 matrix to be displayed on
a panel from the pygame library
'''
def writeimage():
	#print(buf)
	for y in range(0, image_height):
		i = 0
		for x in range(0, image_width):
			color = rgbToPic(int(buf[y][i]), int(buf[y][i+1]))
			i+=2
			screen.set_at((x,y),color)


'''
MAIN FUNCTION
'''
if __name__ == '__main__':
	image_width = 175
	image_height = 128

	buf = readPic()
	
	screen = pygame.display.set_mode((image_width, image_height))
	clock = pygame.time.Clock()
	# Display the panel forever until a key is pressed
	running = True
	while running:
		writeimage()
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				running = False
			elif (event.type == pygame.KEYDOWN):
				if (event.key == pygame.K_SPACE):
					pass
			pygame.display.flip()
			clock.tick(240)



