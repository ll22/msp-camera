%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%   The input raw data from CCs should be in CAMERA.txt file.
% Dimensions for CIF image format. 
ROWS = 144;
COLUMNS = 175;
fid = fopen('mem_TI.dat' , 'r');
raw_image = fscanf(fid , '%d');
mat_image = zeros(ROWS , COLUMNS);
for i = 1:ROWS
    for j = 1:COLUMNS
        mat_image(i , j) = raw_image((i - 1) * COLUMNS + j);
        
    end
end
%max_rows = max(transpose(mat_image));
%for i = 1:ROWS
%    for j = 1:COLUMNS
%        if(max_rows(i) ~= 0)
%            mat_image(i , j) = (mat_image(i , j)*255)/max_rows(i);
%        end
%    end
%end
fclose(fid);
imshow(mat2gray(mat_image));