//working DMA edge trigger

#include "driverlib.h"

//useful URL: https://e2e.ti.com/support/microcontrollers/msp430/f/166/t/335859
//source addr: P4IN
//destination addr: FRAM starting at
//Transfer mode: Single Transfer
//Address mode: FA -> BA
void DMA_Conf()
{

	  P1SEL1 |= BIT0;
	  P1SEL0 &= ~(BIT0); //pinmux p1.0 to be DMA trigger pin

	  // Configure DMA channel 0
	  //__data16_write_addr((unsigned short) &DMA0SA,(unsigned long) 0x1C20);
	  __data16_write_addr((unsigned short) &DMA0SA, 0x0221);
	                                            // Source block address
	  //__data16_write_addr((unsigned short) &DMA0DA,(unsigned long) 0x1C40);
	  __data16_write_addr((unsigned short) &DMA0DA, 0x4500);
	                                            // Destination single address
	  DMA0SZ = 5;                              // Block size
	  //DMA0CTL
	  DMA0CTL = DMADT_0 | DMASRCINCR_0 | DMADSTINCR_3 | DMASRCBYTE | DMADSTBYTE; // Rpt, inc

	  //DMA_TRIGGERSOURCE_26
	  //DMAIV |= 0x02;

	  DMACTL0 |= DMA0TSEL_31;//0x01; //try trigger source 0

	  //set edge-sensitive inerrupt
	  DMA0CTL &= ~(DMALEVEL); //edge sensitive
	  DMA0CTL |= DMAIE; //interrupt enable


	  DMA0CTL |= DMAEN;                         // Enable DMA0
	  //trigger
	  //DMA0CTL |= DMAREQ;                      // Trigger block transfer
	  __bis_SR_register(GIE);
}


int counter = 0;
void main(void)
{
    //Stop Watchdog Timer
    WDT_A_hold(WDT_A_BASE);

    /*
     * Disable the GPIO power-on default high-impedance mode to activate
     * previously configured port settings
     */
    PMM_unlockLPM5();

    	P4DIR &= ~(0xFF); //port 4 all as input

    	DMA_Conf(); //configure DMA
    while(1)
    {

    }
}


//ISRs
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=DMA_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(DMA_VECTOR)))
#endif
void DMA_ISR(void) //after the DMA0SZ counter counts to 0, this interrupt happens
{
	DMAIV &= ~(0x02); //clear the flag
	DMA_Conf(); //reinitialize DMA channel 0 and reload the DMA0SZ register
}

